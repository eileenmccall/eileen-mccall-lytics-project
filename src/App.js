import { useEffect, useState } from 'react';
import './App.css';

function App () {
    // books data
    var [ data, setData ] = useState();
    // mocking async data fetch
    var [ loading, setLoading ] = useState(true);
    // highest count of all genres, used to determine bar width 
    var [ maxCount, setMaxCount ] = useState(1);

    var [ genreInput, setGenreInput ] = useState("");

    // generic data to use on first page load or data reset
    var dummyData = {
        title: "Books",
        rowLabel: "Genres",
        numLabel: "Number of Books",
        rows: [
            { label: "Fantasy", count: 15 },
            { label: "Non-Fiction", count: 5 },
            { label: "Sci-Fi", count: 10 },
            { label: "History", count: 32 },
        ]
    };

    useEffect(function getDataEffect () {
        // check to see if existing data has been saved
        var existingData = JSON.parse(window.localStorage.getItem("booksData"));

        if (existingData) {
            // if yes, set state and mock load
            setData(existingData);
            setTimeout(() => setLoading(false), 2000);
        } else {
            // if not, update localstorage, set state, and mock load
            window.localStorage.setItem("booksData", JSON.stringify(dummyData));
            setData(dummyData);
            setTimeout(() => setLoading(false), 2000);
        }
    }, []);

    useEffect(function updateMaxCountEffect () {
        // every time data gets updated, and if there are rows to check
        if (data && data.rows.length > 0) {
            // get the largest count from all the rows
            var max = data.rows.reduce(function determineMaxCount (acc, cur) {
                return cur.count > acc
                    ? cur.count
                    : acc;
            }, 0);

            setMaxCount(max);
        }
    }, [ data ])

    if (loading) {
        return (
            <div className="app">
                <h1 className="loading">Loading...</h1>
            </div>
        );
    }

    return (
        <div className="app">
            <header>
                <h1 className="header">{ data.title }</h1>
            </header>
            <main>
                <div className="add-genre">
                    <button className="reset-data" onClick={ resetData }>Reset Data</button>
                    <div>
                        <label className="add-genre__label" htmlFor="add-genre-input"><small>Add Genre</small></label>
                        <input className="add-genre__input" type="text" id="add-genre-input" value={genreInput} onChange={(e) => setGenreInput(e.target.value)} />
                        <button onClick={addGenre}>Add</button>
                    </div>
                </div>
                <table className="table">
                    <thead>
                        <tr>
                            <th className="label-header">{ data.rowLabel }</th>
                            <th></th>
                            <th className="count-header">{ data.numLabel }</th>
                            <th className="add-header">Add Book</th>
                            <th className="remove-header">Remove Book</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.rows.length >= 1
                            ? data.rows.map(function (genre) {
                                return (
                                    <tr key={genre.label} className="item">
                                        <td className="item-label">{ genre.label }</td>
                                        {/* bar width % is count divided by maxCount (determined above) */}
                                        <td><div className="item-bar" style={{ width: `${Math.floor(genre.count / maxCount * 100)}%`}}></div></td>
                                        <td className="item-count">{ genre.count }</td>
                                        <td className="item-add"><button onClick={() => addItem(genre.label)}>Add</button></td>
                                        <td className="item-remove"><button onClick={() => removeItem(genre.label)}>Remove</button></td>
                                    </tr>
                                );
                            })
                            : (<tr>
                                <td className="no-books" colSpan={4}><p>No genres in bookshelf</p></td>
                            </tr>)
                        }
                    </tbody>
                </table>
            </main>
            <hr className="divider"/>
            <footer className="footer">
                <p>Built by <a href="https://www.linkedin.com/in/eileenmccall/">Eileen McCall</a> for <a href="https://www.lytics.com/">Lytics</a>, 2022</p>
                <p><a href="https://gitlab.com/eileenmccall/eileen-mccall-lytics-project">Source code here</a></p>
            </footer>
        </div>
    );

    function addItem (label) {
        // find the index of the row I want to update
        var index = data.rows.findIndex(el => el.label === label);

        // if index is found
        if (index !== -1) {
            // get found row
            let rowToUpdate = data.rows[index];

            // make a copy of old rows
            let rows = [ ...data.rows ];

            // update row in the copy
            rows[index] = {
                label: rowToUpdate.label,
                count: ++rowToUpdate.count
            };

            // make an updated copy of data
            let updatedData = {
                ...data,
                rows
            };

            // update state with copy
            window.localStorage.setItem("booksData", JSON.stringify(updatedData));
            setData(updatedData);
        }
    }

    function removeItem (label) {
        console.log(label);
        // find the index of the row I want to update
        var index = data.rows.findIndex(el => el.label === label);

        // if index is found
        if (index !== -1) {
            // get found row
            let rowToUpdate = data.rows[index];

            // make a copy of old rows
            let rows = [ ...data.rows ];

            // if this is the last book to remove
            if (rowToUpdate.count === 1) {
                // remove that row entirely
                rows = rows.filter(val => val.label !== label);
            } else {
                // otherwise update row in the copy
                rows[index] = {
                    label: rowToUpdate.label,
                    count: --rowToUpdate.count
                };
            }

            // make an updated copy of data
            let updatedData = {
                ...data,
                rows
            };

            // update state with copy
            window.localStorage.setItem("booksData", JSON.stringify(updatedData));
            setData(updatedData);
        }
    }

    function addGenre () {
        var updatedData = {
            ...data,
            rows: [
                ...data.rows,
                { label: genreInput, count: 0}
            ]
        };

        localStorage.setItem("booksData", JSON.stringify(updatedData));
        setData(updatedData);
        setGenreInput("");
    }

    function resetData () {
        localStorage.setItem("booksData", JSON.stringify(dummyData));
        setData(dummyData);
        setGenreInput("");
    }
}

export default App;
